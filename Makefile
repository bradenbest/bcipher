CFLAGS = -Wall -Wextra -std=c99 -O3

all: bcipher bdecipher

bcipher: bcipher.o
	$(CC) $^ -o $@

bdecipher: bdecipher.o
	$(CC) $^ -o $@

clean:
	rm -f *.o bcipher bdecipher

.PHONY: clean all
