a basic rotation cipher only has a period of 26, because you can
decipher it in 26 steps at most

I made [this cipher](./qwerty-cipher.js) in javscript as a joke, but
then I noticed it has a period of 630, as the rows, which are of length
7, 9, and 10, "desync" and multiply together.

I thought about it and realized you could break 26 apart in more ways to
create an even larger product, but to optimize it for period length,
you'd have to avoid terms that are divisible by each other,

    2/4
    0 1
    0 2 3 4
    00 -> 12 -> 03 -> 14 -> 00
    period = 4, not 8

meaning prime numbers are ideal candidates for the so-called "chain
lengths". A chain being a sequence of mappings that forms a loop.

I experimented by adding the first several primes and landed on

2+3+5+7+11+13+17+19+23+29+31+37+41 = 238, which leaves 18, which could
be an 18-length chain or 18 1-length chains (characters that map to
themselves).

This will have a period of (multiply all the primes together) =
304250263527210 (304 trillion). Which is quite an impressive period for
a caesar cipher and actually means the cipher can be keyed (the key
would be the number of times it's shifted).

Thus the mapping should include the following chains:

2, 3, 5, 7, 11, 13, 17, 18, 19, 23, 29, 31, 37, 41

I came up with this simple series of mappings

    18: 00 .. 11
    2:  12 .. 13
    3:  14 .. 16
    5:  17 .. 1B
    7:  1C .. 22
    11: 23 .. 2D
    13: 2E .. 3A
    17: 3B .. 4B
    19: 4C .. 5E
    23: 5F .. 75
    29: 76 .. 92
    31: 93 .. B1
    37: B2 .. D6
    41: D7 .. FF

The mappings could be changed to change the cipher, though the ranges
must be contiguous. I initially tried to map non-contiguously, but I
decided to go for this much simpler design. Not because it's "too hard".
Because there are 256 mapping pairs to consider and this project is for
fun. Specifying the mappings non-contiguously would be very tedious.

Do note that the 304 trillion figure only applies if the input text has
characters that span across all 14 "classes" or mapping chains. For
example, if your message is just "HI", then you'll only have a period of
17 since those characters both fall in a single class: the 17 chain
(`17: 3B .. 4B`). "HELLO WORLD" would have a longer period, since its
characters span across the 7, 17 and 19 chains for a period of 2261.
Which is to say, if someone encountered this in the wild:

    00000000: 423f 4e4e 511d 5951 544e 3e07            B?NNQ.YQTN>.

and happened to know it was generated with this program, they could
brute force the key in a maximum of 2261 tries. Plain text will
inherently touch only a fraction of the chains since that accounts for
just under 100 characters that span the 18, 7, 11, 13, 17, 19, 23 and 29
classes for a period of 2x3x7x11x13x17x19x23x29 = 1293938646 (1.2
million). The 2 and 3 are there because they are unique prime factors of
18.

Since binary files span the entire range of a byte, they are more likely
to span all 14 classes and thus more likely to have a cipher period of
304 trillion.

The reason this is weak compared to the likes of RSA is of course that
304 trillion isn't a very large number in computer terms. You could
brute force a key in a matter of minutes. If somebody encounters a file
enciphered with this algorithm and is aware of that fact, it wouldn't
offer much resistance.
