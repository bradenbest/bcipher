const qwerty_cipher = (function(){
    const charmap = "snvfrghjoklazmpqwtdyibecux";

    function cipherchar(ch){
        let charcode = ch.charCodeAt(0);

        if (charcode >= 0x61 && charcode <= 0x7a)
            return charmap[charcode - 0x61];

        return ch;
    }

    function decipherchar(ch){
        let idx = charmap.indexOf(ch);

        if (idx === -1)
            return ch;

        return String.fromCharCode(idx + 0x61);
    }

    function cipher(text, cipherfn){
        return text
            .toLowerCase()
            .split("")
            .filter(x => x)
            .map(cipherfn)
            .join("");
    }

    return Object.seal({
        encipher: text => cipher(text, cipherchar),
        decipher: text => cipher(text, decipherchar),
    });
}());
