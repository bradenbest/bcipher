# bcipher - Braden's Cipher

Simple Caesar cipher with over 300 trillion possible keys.

## Build

To build, simply run make

## Usage

bcipher comes as two programs: bcipher and bdecipher. The key is passed in as an argument.

```sh
make
./bcipher 17
Hello
H_ffi
```

Input is fed in via stdin and output is printed to stdout

```sh
./bcipher 479876895 < file > file-ciphered
```
