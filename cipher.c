#define NDEBUG
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

typedef uint8_t  u8;
typedef uint64_t u64;
typedef int16_t  i16;

struct mapchain {
    u8 low;
    u8 high;
    u8 length;
};

static inline struct mapchain const *  mapchain_find  (u8 ch);
static inline u8                       cipher_char    (u8 ch, u64 key);

void  cipher_file  (FILE *f, u64 key);

static struct mapchain const mapchains[14] = {
    { 0x00, 0x11, 18 },
    { 0x12, 0x13, 2  },
    { 0x14, 0x16, 3  },
    { 0x17, 0x1B, 5  },
    { 0x1C, 0x22, 7  },
    { 0x23, 0x2D, 11 },
    { 0x2E, 0x3A, 13 },
    { 0x3B, 0x4B, 17 },
    { 0x4C, 0x5E, 19 },
    { 0x5F, 0x75, 23 },
    { 0x76, 0x92, 29 },
    { 0x93, 0xB1, 31 },
    { 0xB2, 0xD6, 37 },
    { 0xD7, 0xFF, 41 },
};

static inline struct mapchain const *
mapchain_find(u8 ch)
{
    for (int i = 0; i < 14; ++i)
        if (ch >= mapchains[i].low && ch <= mapchains[i].high)
            return mapchains + i;

    return NULL; // unreachable
}

/* 1. see bciper.c, bdecipher.c
 */
static inline u8
cipher_char(u8 ch, u64 key)
{
    struct mapchain const *  selchain;
    u8                       adjusted_key;
    i16                      ch_sig;

    selchain = mapchain_find(ch);
    assert(selchain != NULL);
    adjusted_key = key % selchain->length;
    ch_sig = ch;

#if CIPHER_MODE == CIPHER_ADD // 1
    ch_sig += adjusted_key;

    if (ch_sig > selchain->high)
        ch_sig -= selchain->length;
#else // CIPHER_SUBTRACT
    ch_sig -= adjusted_key;

    if (ch_sig < selchain->low)
        ch_sig += selchain->length;
#endif

    return (u8)ch_sig;
}

void
cipher_file(FILE *f, u64 key)
{
    int  ch;

    while ((ch = fgetc(f)) != EOF)
        putchar(cipher_char((u8)ch, key));
}

static inline void
usage(void)
{
    puts("bcipher - braden's cipher");
    puts("usage: bcipher <key>");
    puts("       bdecipher <key>");
    puts("key: number between 0 and 2^64-1");
    puts("     reads input from stdin and prints output to stdout");
    puts("example: echo H_ffi | bdecipher 17");
    exit(1);
}

int
main(int argc, char **argv)
{
    u64  key;

    if (argc < 2)
        usage();

    key = strtoul(argv[1], NULL, 10);
    cipher_file(stdin, key);
    return 0;
}
